package com.nisum.dao;

import org.springframework.data.repository.CrudRepository;

import com.nisum.model.Libro;

public interface LibrosDao extends CrudRepository<Libro, Long> {
	
	public Libro findByTitulo(String titulo);

}
