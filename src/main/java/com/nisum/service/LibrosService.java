package com.nisum.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.nisum.dao.LibrosDao;
import com.nisum.model.Libro;

public class LibrosService {
	//inyección clase
	@Autowired
	private  LibrosDao libroDao;
	
	/*
	 private final LibrosDao libroDao
	 @Autowired
	 public LibrosService(LibrosDao libroDa{
	 	this.librosDao = libroDao;
	 }
	 
	 */
	
	public  Libro crearLibro(Libro libro) {
		// TODO Auto-generated method stub
		return libroDao.save(libro);
	}

	public Libro obtenerLibro(long id) {
		// TODO Auto-generated method stub
		Libro libro = new Libro();
		libro = libroDao.findOne(id);
		return libro;
	}

	public List<Libro> obtenerTodos() {
		// TODO Auto-generated method stub
		List<Libro> misLibros = new ArrayList<Libro>();
		
		misLibros = (ArrayList<Libro>) libroDao.findAll();
		
		return misLibros;
	}

	public Libro obtenerLibro(String titulo) {
		// TODO Auto-generated method stub
		Libro libro = new Libro();
		libro = libroDao.findByTitulo(titulo);
		return libro;
	}

}
